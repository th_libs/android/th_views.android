package com.th.android.views;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

/**
 * It's a simple view that flow all the child
 *
 * It horizontal it's child and to next when it's have no enough space for the child.
 */
public class FlowView extends FrameLayout  {
    private static final String TAG = FlowView.class.getSimpleName();

    public FlowView(@NonNull Context context) {
        super(context);
    }

    public FlowView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FlowView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public FlowView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // now tell the child
        // width is match parent
        int height = 0;
        int currentLineWidth = 0;
        int currentLineHeight = 0;

        // yes the width is 1080
        int width = MeasureSpec.getSize(widthMeasureSpec);
        if (width <= 0) {
            throw new IllegalStateException(TAG + " need an clearly width");
        }

        boolean isLineFirstChild = true;
        // ok we need more control at here.
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            ViewGroup.LayoutParams params = child.getLayoutParams();
            if (params.width == ViewGroup.LayoutParams.MATCH_PARENT
                    || params.height == ViewGroup.LayoutParams.MATCH_PARENT) {
                Log.e(TAG, String.format("%s's child's layoutParam can't be match_parent", this.getClass().getName()));
            }

            // java.lang.ClassCastException: android.view.ViewGroup$LayoutParams cannot be cast to android.view.ViewGroup$MarginLayoutParams
            // 03-22 13:39:19.052 16262 16262 E AndroidRuntime: 	at android.view.ViewGroup.measureChildWithMargins(ViewGroup.java:6948)
            // 03-22 13:39:19.052 16262 16262 E AndroidRuntime: 	at android.widget.FrameLayout.onMeasure(FrameLayout.java:194)
            child.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
                    , MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

            // we didn't handle the first line.
            currentLineWidth += child.getMeasuredWidth();

            if (!isLineFirstChild && currentLineWidth > width) { // need next line
                isLineFirstChild = true;
                // add previous line
                height += currentLineHeight;

                currentLineWidth = child.getMeasuredWidth();
                currentLineHeight = child.getMeasuredHeight();
            } else {
                isLineFirstChild = false;
                currentLineHeight = Math.max(currentLineHeight, child.getMeasuredHeight());
            }

            // add current Line
            height += currentLineHeight;

        }
        setMeasuredDimension(width, height);
    }

    private void layoutChild(int left, int top, View child) {
        int right = left + child.getMeasuredWidth();
        int bottom = top + child.getMeasuredHeight();

        Log.i(TAG, String.format("layout child at [%d, %d, %d, %d]", left, top, right, bottom));
        child.layout(left, top, right, bottom);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        // do the previous again??
        // 这就是安卓设计不同，像flutter直接把onMeasure和layout放到一起
        int width = right - left;
        int lineMaxHeight = 0;
        int curTop = top;
        int curLeft = 0;

        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            boolean isFirstChild = i == 0;
            if (isFirstChild || (curLeft + child.getMeasuredWidth()) < width) {
                // 可以放了
                layoutChild(curLeft, curTop, child);
                curLeft += child.getMeasuredWidth();
                lineMaxHeight = Math.max(lineMaxHeight, child.getMeasuredHeight());
            } else { // 换行放
                curTop += lineMaxHeight;
                curLeft = 0;

                layoutChild(curLeft, curTop, child);

                curLeft += child.getMeasuredWidth();
                lineMaxHeight = child.getMeasuredHeight();
            }
        }
    }
}
