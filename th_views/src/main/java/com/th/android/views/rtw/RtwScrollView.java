package com.th.android.views.rtw;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.widget.ScrollView;

import androidx.annotation.RequiresApi;

/**
 * 简易版本ScrollView
 * // now let add the velocity tracker
 * The OverScroller is strange. We just hold on here.
 */
public class RtwScrollView extends ScrollView {
    private static final String TAG = RtwScrollView.class.getSimpleName();
    int mOverScrollDistance;
    private VelocityTracker mVelocityTracker;
    private int mMinimumVelocity;
    private int mMaximumVelocity;

    public RtwScrollView(Context context) {
        this(context, null);
    }

    public RtwScrollView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RtwScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public RtwScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    void init() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(this.getContext());
        this.mOverScrollDistance = viewConfiguration.getScaledOverscrollDistance();
        this.mMinimumVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
        this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
        // when did I return back?
        this.mVelocityTracker = VelocityTracker.obtain();
    }

    private void checkChildCount() {
        if (this.getChildCount() > 1) {
            Log.w(TAG, "RtwScroll can only have on child");
            throw new IllegalStateException("RtwScroll can only have on child");
        }
    }

    double lastY = 0;

    @Override
    public void computeScroll() {
        // it's some complicated.
//        Log.i(TAG, "computeScroll called from: " + Log.getStackTraceString(new Throwable()));
        // most called by draw. So If we trigger an scroll at here, that will continue trigger layout, so can implements fling
        super.computeScroll();
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        // ok how can we do this?
        // we use the View's overScrollBy to the scroll.
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastY = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                mVelocityTracker.addMovement(ev);
                double dY = ev.getY() - lastY;
                boolean hitBoundary = overScrollBy(
                        0,
                        -(int)dY,
                        0,
                        // how to remember the already scrolled??
                        getScrollY(),
                        0,
                        (int)getScrollRange(),
                        0,
                        this.mOverScrollDistance,
                        true

                );
                if (hitBoundary) {
                    mVelocityTracker.clear();
                }
                lastY = ev.getY();
                break;
            case MotionEvent.ACTION_UP:
                mVelocityTracker.addMovement(ev);
                mVelocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
                float yVelocity = mVelocityTracker.getYVelocity();
                if (yVelocity > mMinimumVelocity) {
                    fling(yVelocity);
                }
                Log.i(TAG, "yVelocity: " + yVelocity);
                // now let's take the
                break;
        }
        return true;
    }

    /**
     * child height - this.height is the scroll range.
     */
    private double getScrollRange() {
        double range = 0;
        if (this.getChildCount() > 0) {
            range = Math.max(0, this.getChildAt(0).getHeight() - (this.getHeight() - this.getPaddingTop() - this.getPaddingBottom()));
        }
        return range;
    }

    // fling is after up touch, that still need scroll for a while
    private void fling(double velocityY) {
        // how can we fling with the initial speed velocity.
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }
}
