package com.th.android.views

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.ImageView

@SuppressLint("AppCompatCustomView")
/**
 * For now, we just support set the height by width
 */
class FixedRatioImageView: ImageView {
    private var ratioHW = -1.0f

    companion object {
        var TAG = FixedRatioImageView::class.simpleName
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val a = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.FixedRatioImageView,
                0, 0)
        try {
            Log.i(TAG, "before get ratio")
            this.ratioHW = a.getFloat(R.styleable.FixedRatioImageView_radioHW, -1.0f);
            Log.i(TAG, "after before get ratio: $ratioHW")
        } finally {
            a.recycle()
        }
    }

    fun setRatioHW(ratioHW: Float) {
        this.ratioHW = ratioHW
        requestLayout()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        Log.i(TAG, "measuredWidth: ${measuredWidth}, measuredHeight: $measuredHeight, ratioHW: $ratioHW")

         if (measuredWidth > 0 && this.ratioHW > 0) {
             this.setMeasuredDimension(measuredWidth, (measuredWidth * this.ratioHW).toInt())
             Log.i(TAG, "after change measure measuredWidth: ${measuredWidth}, measuredHeight: $measuredHeight")
         }
    }

}