package com.th.android.views;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

/**
 * This is the just an test class, you should not use this.
 *
 * The scroll is already better handled by the view it self.
 *
 * 这个ScrollView和别的系统ScrollView不一样，他接受多个child，并把child按顺序排列
 */
public class MultiChildScrollView extends FrameLayout  {
    private static final String TAG = MultiChildScrollView.class.getSimpleName();

    public MultiChildScrollView(@NonNull Context context) {
        super(context);
    }

    public MultiChildScrollView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MultiChildScrollView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MultiChildScrollView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // we can't get parent height?
        int width = MeasureSpec.getSize(widthMeasureSpec);
        boolean useParentWidth = width > 0;
        int height = 0;

        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.measure(
                    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
            );

            if (!useParentWidth) {
                width = Math.max(width, child.getMeasuredWidth());
            }

            height += child.getMeasuredHeight();
        }

        setMeasuredDimension(width, height);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int top = (int) scrollOffset;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.layout(0, top, child.getMeasuredWidth(), top + child.getMeasuredHeight());
            top += child.getMeasuredHeight();
        }
    }

    float beginX = -1;
    float beginY = -1;

    float scrollOffset = 0;
    float currentOffset = 0;


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            beginX = event.getX();
            beginY = event.getY();
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (beginY > 0) {
                scrollOffset = currentOffset + (event.getY() - beginY);
                requestLayout();
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
            beginX = -1;
            beginY = -1;
            currentOffset = scrollOffset;
        }

        return true;
    }

}
