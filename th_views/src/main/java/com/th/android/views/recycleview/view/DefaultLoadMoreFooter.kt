package com.th.android.views.recycleview.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import com.th.android.views.R
import com.th.android.views.databinding.ThViewsDefaultLoadMoreFooterBinding

/**
 * And I need recycleView state too.
 */
open class DefaultLoadMoreFooter: FrameLayout {
    private lateinit var mBinding: ThViewsDefaultLoadMoreFooterBinding

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.th_views_default_load_more_footer, this, true)
        showNoMoreData()
    }

    fun showLoading() {
        mBinding.pb.visibility = View.VISIBLE
        mBinding.tv.text = "加载中..."
    }

    fun showLoadFinish() {
        mBinding.pb.visibility = View.GONE
        mBinding.tv.text = "加载完成"
    }

    fun showNoMoreData() {
        mBinding.pb.visibility = View.GONE
        mBinding.tv.text = "无更多数据~"
    }
}
