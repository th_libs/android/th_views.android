package com.th.android.views.recycleview

import android.content.Context
import android.content.res.Resources
import android.graphics.Rect
import android.util.Log
import android.util.TypedValue
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

class CommonSpacing(spacingHorizontalInDp: Int, spacingVerticalInDp: Int, context: Context) : ItemDecoration() {

    companion object {
        var TAG = CommonSpacing::class.simpleName

        private fun dp2px(dp: Float, context: Context): Float {
            val r: Resources = context.resources
            return TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    dp,
                    r.displayMetrics
            )
        }
    }

    private var spacingH: Int = dp2px(spacingHorizontalInDp.toFloat(), context).toInt()
    private var spacingV: Int = dp2px(spacingVerticalInDp.toFloat(), context).toInt()
    private var spacingHHalf: Int = (dp2px(spacingHorizontalInDp.toFloat(), context) / 2).toInt()
    private var spacingVHalf: Int = (dp2px(spacingVerticalInDp.toFloat(), context) / 2).toInt()

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val layoutManager = parent.layoutManager
        if (layoutManager is GridLayoutManager) {
            val pos = parent.getChildLayoutPosition(view)
            val spanCount = layoutManager.spanCount
            val isLeft = (pos % spanCount) == 0
            val isTop = pos < spanCount
            val isRight = (pos + 1) % spanCount == 0

            // 做简单一点
            var left = spacingHHalf
            var right = spacingHHalf
            var top = spacingVHalf
            var bottom = spacingVHalf

            if (isLeft) {
                left = 0
            }

            if (isRight) {
                right = 0
            }

            if (isTop) {
                top = 0
            }

            // TODO: how to judge the bottom

            outRect.set(left, top, right, bottom)
        } else {
            Log.w(TAG, "${CommonSpacing::class.simpleName} for now can only work on grid")
            super.getItemOffsets(outRect, view, parent, state)
        }
    }
}