package com.th.android.views.load;

import android.content.Context;
import android.util.AttributeSet;

import androidx.recyclerview.widget.RecyclerView;

public class LoadMoreRecycleView extends RecyclerView {
    private boolean hasMoreData = false;

    public LoadMoreRecycleView(Context context) {
        this(context, null);
    }

    public LoadMoreRecycleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadMoreRecycleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
//        TypedArray a = context.getTheme().obtainStyledAttributes(
//                attrs,
//                R.styleable.LoadMoreRecycleView,
//                0, 0);
//
//        try {
//
//        } finally {
//            a.recycle();
//        }
    }

    public void setNoMoreDate() {
        this.hasMoreData = false;
    }
}
