package com.th.android.views.load

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.th.android.views.R

class LoadView: FrameLayout {
    var loadingView: View? = null
    var emptyView: View? = null
    var errorView: View? = null
    var dataView: View? = null

    companion object {
        val TAG = LoadView::class.simpleName
        const val STATE_LOADING = 1
        const val STATE_EMPTY = 2
        const val STATE_ERROR = 3
        const val STATE_DATA = 4
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        // at here can't get child count
        // which the LoadView is create in xml
        Log.i(TAG, "child Count: $childCount")

        // can do like this. I think
        val a = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.LoadView,
                0, 0)

        try {
            var layoutLoading = a.getResourceId(R.styleable.LoadView_layout_loading, -1)
            if (layoutLoading <= 0) {
                layoutLoading = R.layout.th_views_view_default_loading;
            }

            if (layoutLoading > 0) {
                // 这里要注意，要传false，否则拿不到新生成的view
                this.loadingView = LayoutInflater.from(getContext()).inflate(layoutLoading, this, false)
                this.addView(this.loadingView);
            }
        } finally {
            a.recycle()
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        findChildView()
    }


    fun showLoading() {
        hideAll()
        loadingView?.visibility = View.VISIBLE
    }

    fun showEmpty() {
        hideAll()
        emptyView?.visibility = View.VISIBLE
    }

    fun showError() {
        hideAll()
        errorView?.visibility = View.VISIBLE
    }

    fun showData() {
        hideAll()
        dataView?.visibility = View.VISIBLE
    }

    private fun hideAll() {
        findChildView()
        Log.i(TAG, "hideAll child View: $dataView, hash: ${dataView?.hashCode()}")
        loadingView?.visibility = View.GONE
        emptyView?.visibility = View.GONE
        errorView?.visibility = View.GONE
        dataView?.visibility = View.GONE
    }

    private fun findChildView() {
        Log.i(TAG, "childCount: $childCount")
        if (this.dataView == null) {
            if (this.childCount > 1) {
                Log.i(TAG, "children: " + getChildAt(0))
                Log.i(TAG, "children: " + getChildAt(1))
            }
            for (i in 0..childCount) {
                val child = getChildAt(i)
                if (child != this.loadingView
                        && child != this.errorView
                        && child != this.emptyView) {
                    this.dataView = child
                    break
                }
            }
        }
    }

}