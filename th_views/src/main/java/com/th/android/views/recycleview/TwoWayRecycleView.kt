package com.th.android.views.recycleview

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.th.android.views.recycleview.view.DefaultLoadMoreFooter
import java.util.concurrent.atomic.AtomicBoolean

/**
 * 我调用onLoadMoreListener之后。一定要被调用
 *
 * How to handle the layout manager??
 */
class TwoWayRecycleView: RecyclerView {
    private var isNoData = false
    private var onLoadMoreListener: (() -> Void)? = null
    private var isLoadingMore: AtomicBoolean = AtomicBoolean(false)
    var canLoadMore: Boolean = true
    private var hasInit = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        addOnScrollListener(object: OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (!isNoData && canLoadMore && newState == SCROLL_STATE_IDLE && layoutManager != null
                        && (layoutManager is LinearLayoutManager)) {
                    val itemCount = layoutManager?.itemCount?: 0
                    val childCount = layoutManager?.childCount?: 0
                    val lastVisibleItemPosition = (layoutManager as? LinearLayoutManager)?.findLastVisibleItemPosition()?: 0

                    // for now check childCount < itemCount
                    if (childCount < itemCount && lastVisibleItemPosition == itemCount - 1) {
                        triggerLoadMore()
                    }
                }
            }
        })
    }

    fun notifyLoadMoreFinish(hasMoreData: Boolean = true) {
        this.isLoadingMore.set(false)
        if (!hasMoreData) {
            setNoMoreData()
        } else {
            setAdapterState(LOAD_MORE_FINISH)
        }
    }

    fun setNoMoreData() {
        isNoData = true
        setAdapterState(LOAD_MORE_NO_MORE_DATA)
    }

    fun setOnLoadMoreListener(onLoadMoreListener: () -> Void) {
        this.onLoadMoreListener = onLoadMoreListener
    }

    private fun triggerLoadMore() {
        if (!this.isLoadingMore.get()) {
            setAdapterState(LOAD_MORE_LOADING)
            this.isLoadingMore.set(true)
            this.onLoadMoreListener?.invoke()
        }
    }

    override fun setLayoutManager(layout: LayoutManager?) {
        super.setLayoutManager(layout)
        if (layout != null && layout is GridLayoutManager) {
            val originSizeLookup = layout.spanSizeLookup
            layout.spanSizeLookup = object: GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (position == layout.itemCount -1 ) {
                        layout.spanCount
                    } else {
                        originSizeLookup?.getSpanSize(position)?: 1
                    }
                }
            }
        }
    }

    override fun setAdapter(adapter: Adapter<ViewHolder>?) {
        if (adapter == null) return
        super.setAdapter(TwoWayRecycleViewAdapter(adapter))
    }

    private fun setAdapterState(state: Int) {
        (adapter as? TwoWayRecycleViewAdapter)?.setState(state)
    }

    companion object {
        const val LOAD_MORE_LOADING = 1
        const val LOAD_MORE_FINISH = 2
        const val LOAD_MORE_NO_MORE_DATA = 3

        /**
         * How to choice the view type??
         * I need recycleView state?
         */
        private class TwoWayRecycleViewAdapter(val innerAdapter: Adapter<ViewHolder>): Adapter<ViewHolder>() {
            private var state = LOAD_MORE_LOADING

            companion object {
                const val BEGIN_VIEW_TYPE = 2 shl 8
            }

            fun setState(newState: Int) {
                if (newState == state) {
                    return
                }

                state = newState
                // how to just change one view?
                notifyDataSetChanged()
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
                return if (viewType != choiceViewType()) {
                    innerAdapter.createViewHolder(parent, viewType)
                } else {
                    val defaultLoadMoreFooter = DefaultLoadMoreFooter(parent.context)
                    defaultLoadMoreFooter.layoutParams = LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    // why you wrong??
                    DefaultLoadMoreVH(defaultLoadMoreFooter)
                }
            }

            override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                if (holder is DefaultLoadMoreVH) {
                    val root = holder.root
                    when (state) {
                        LOAD_MORE_LOADING -> {
                            root.showLoading()
                        }
                        LOAD_MORE_FINISH -> {
                            root.showLoadFinish()
                        }
                        else -> {
                            root.showNoMoreData()
                        }
                    }
                } else {
                    innerAdapter.onBindViewHolder(holder, position)
                }
            }

            override fun getItemCount(): Int {
                return if (hasLoadMoreFooter()) {
                    innerAdapter.itemCount + 1
                } else {
                    innerAdapter.itemCount
                }
            }

            override fun getItemViewType(position: Int): Int {
                return if (hasLoadMoreFooter() && position == innerAdapter.itemCount) {
                    choiceViewType()
                } else {
                    innerAdapter.getItemViewType(position)
                }
            }

            /**
             * For now just pick a very big value.
             */
            private fun choiceViewType(): Int {
                return BEGIN_VIEW_TYPE
            }

            private fun hasLoadMoreFooter(): Boolean {
                return true
            }
        }

        private class DefaultLoadMoreVH(root: DefaultLoadMoreFooter) : SingleTopViewHolder<DefaultLoadMoreFooter>(root) {
        }
    }

}