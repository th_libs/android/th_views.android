package com.th.android.views.recycleview

import android.view.View
import androidx.recyclerview.widget.RecyclerView

open class SingleTopViewHolder<T: View>(val root: T) : RecyclerView.ViewHolder(root) {
}