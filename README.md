# 如何引用
- 假设项目为my_app。在my_app/settings.gradle中添加

```
include ":fly_views"
project(":fly_views").projectDir = new File(settingsDir, "相对路径/fly_views.android/thyi")
```

- 在my_app/app/build.gradle中引用

```
implementation(project(":fly_views"))
```