package com.th.android.views.demo;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

import androidx.annotation.RequiresApi;

public class NestedScrollView extends ScrollView  {
    public NestedScrollView(Context context) {
        super(context);
    }

    public NestedScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NestedScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NestedScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean rst = super.onInterceptTouchEvent(ev);
        int actionMask = ev.getAction() & MotionEvent.ACTION_MASK;
        if (actionMask == MotionEvent.ACTION_MOVE) {
            // how to judge that target is GridView.
        }
        // 好像这样自己也可以滑动，会走到onTouchEvent
        return false;
    }
}
