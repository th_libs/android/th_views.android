package com.th.android.views.demo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.th.android.views.FlowView;
import com.th.android.views.MultiChildScrollView;
import com.th.android.views.rtw.RtwScrollView;

import java.util.Random;

import static com.th.android.views.demo.Util.createTextViewChild;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, ScrollConflictTestActivity.class));
        finish();
//        setContentView(R.layout.activity_main);
//        testRtwScrollView();
    }

    private void testRtwScrollView() {
        RtwScrollView rtwScrollView = findViewById(R.id.rtwScrollView);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        for (int i = 0; i < 100; i++) {
            linearLayout.addView(createTextViewChild(linearLayout));
        }
        rtwScrollView.addView(linearLayout);
    }

    private void testMultiChildScrollView() {
        MultiChildScrollView multiChildScrollView = findViewById(R.id.multiChildScrollView);
        for (int i = 0; i < 100; i++) {
            multiChildScrollView.addView(createTextViewChild(multiChildScrollView));
        }
    }

    private void testFlowView() {
        FlowView flowView = findViewById(R.id.flowView);
        for (int i = 0; i < 100; i++) {
            flowView.addView(createTextViewChild(flowView));
        }
    }


}