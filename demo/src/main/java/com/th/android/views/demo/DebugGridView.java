package com.th.android.views.demo;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.GridView;

import androidx.annotation.RequiresApi;

public class DebugGridView extends GridView  {
    private static final String TAG = "DebugGridView";
    public DebugGridView(Context context) {
        super(context);
    }

    public DebugGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DebugGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DebugGridView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private String touchAction(int action) {
        if (action == MotionEvent.ACTION_DOWN) {
            return "action_down";
        } else if (action == MotionEvent.ACTION_UP) {
            return "action_up";
        } else if (action == MotionEvent.ACTION_MOVE) {
            return "action_move";
        } else if (action == MotionEvent.ACTION_CANCEL) {
            return "action_cancel";
        }
        return "unknown: " + action;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        // can you get touch down?
        boolean rst = super.onTouchEvent(ev);
        Log.i(TAG, "onTouchEvent(action: " + touchAction(ev.getAction()) + "), rst: " + rst);
        return rst;
    }
}
