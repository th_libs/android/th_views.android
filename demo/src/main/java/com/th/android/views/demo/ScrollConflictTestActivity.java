package com.th.android.views.demo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import static com.th.android.views.demo.Util.createTextViewChild;

/**
 * 如果再一个ScrollView里面包含一个GridView（长度可滑动），那么GridView是否能滑动呢。
 *
 * 很简单的逻辑，刚开始的touchDown事件是给了子GridView，甚至是一些onMove事件，但是后面ScrollView判断到了此事件
 * 不是点击事件，就拦截下来，不继续往下传了。
 *
 * 那么我们怎么破局呢？
 * 我觉得，可以复写ScrollView，如果firstTouchTarget或者链路上有ScrollView的话，就放行吧。
 */
public class ScrollConflictTestActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_conflict);
        LinearLayout linearLayout = findViewById(R.id.ll);
        GridView gv = findViewById(R.id.gv);

        gv.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return 50;
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return createTextViewChild(parent);
            }
        });

        for (int i = 0; i < 30; i++) {
            linearLayout.addView(createTextViewChild(linearLayout));
        }
    }
}
