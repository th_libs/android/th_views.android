package com.th.android.views.demo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Util {
    public static View createTextViewChild(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_flow_view, parent, false);
        TextView tv = view.findViewById(R.id.tv);
        tv.setText(randomWord());
        return view;
    }

    private static String randomWord() {
        StringBuilder sb  = new StringBuilder();
        int times = 1 + (int)(Math.random() * 10);
        for (int i = 0; i < times; i++) {
            sb.append("test");
            if (i != times - 1) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }
}
